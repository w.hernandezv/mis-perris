
const CACHE_NAME = 'v1_mis_perris',
urlsToCache = [
    
    '/static/css/main.css',
    '/static/css/register.css',
    '/static/css/bootstrap.css',
    '/static/js/app.js',
    '/static/js/popper.js',
    '/static/img/icon_512.png',
    '/static/img/favico.png',
    '/cache/',
    '/cache/quiensomos/',
    '/static/img/adoptados/Tom.jpg',
    '/static/img/adoptados/Apolo.jpg',
    '/static/img/adoptados/Duque.jpg',
    '/static/img/fbico.png',
    '/static/img/gplusico.png',
    '/static/img/igico.png',
    '/static/img/mailico.png',
    '/static/img/rescate.jpg',
    '/static/img/crowfunding.jpg',

    '/static/img/rescatados/Oso.jpg',
    '/static/img/rescatados/Maya.jpg',
    '/static/img/rescatados/Wifi.jpg',
    '/static/img/rescatados/Chocolate.jpg',
    '/static/img/rescatados/Pexel.jpg',
    '/static/img/rescatados/Bigotes.jpg',
    '/static/img/rescatados/Luna.jpg',
    '/static/img/Fundacion.jpg'

]

self.addEventListener('install', e => {
e.waitUntil(
    caches.open(CACHE_NAME)
        .then(cache => {
            return cache.addAll(urlsToCache)
                .then(() => self.skipWaiting())
        })
        .catch(err => console.log('Fallo registro de cache', err))
)
})

self.addEventListener('activate', e => {
const cacheWhitelist = [CACHE_NAME]

e.waitUntil(
    caches.keys()
        .then(cacheNames => {
            return Promise.all(
                cacheNames.map(cacheName => {
                    //Eliminamos lo que ya no se necesita en cache
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName)
                    }
                })
            )
        })
        // Le indica al SW activar el cache actual
        .then(() => self.clients.claim())
)
})

//cuando el navegador recupera una url
self.addEventListener('fetch', e => {

e.respondWith(
    caches.match(e.request)
        .then(res => {
            if (res) {
                //recuperar del cache
                return res
            }

            return fetch(e.request)
        })
)
})

