from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    usuario = request.session.get('usuario',None)
    nombre = request.session.get('nombre',None)
    if nombre == '':
        nombre = usuario
    return render(request,'index.html',{'usuario':usuario,'nombre':nombre})

def quiensomos(request):
    usuario = request.session.get('usuario',None)
    nombre = request.session.get('nombre',None)
    if nombre == '':
        nombre = usuario
    return render(request, 'quiensomos.html', {'usuario':usuario,'nombre':nombre})

def contacto(request):
    usuario = request.session.get('usuario',None)
    nombre = request.session.get('nombre',None)
    if nombre == '':
        nombre = usuario
    return render(request, 'contacto.html', {'usuario':usuario,'nombre':nombre})