$(function () {
    $("#formulario").validate({
        rules: {
            correo: {
                required: true,
                email: true
            },
            rut: {
                required: true,
                minlength: 8,
                maxlength: 9,
                rut:true,
                pattern:'[0-9kK]{8,9}'
            },
            nombre: {
                required: true,
                pattern:'([A-zÀ-ž ]){1,}'
            },
            contrasena: {
                required: true,
                minlength: 8
            },
            fechanacimiento: {
                required: true,
                max:'2000-12-31'
            },
            telefono: {
                required: true,
                minlength: 8,
                maxlength: 8
            },
            region: {
                required: true
            },
            comuna: {
                required: true
            },
            tipovivienda: {
                required: true
            }
        },
        messages: {
            correo: {
                required: 'Debe ingresar su correo',
                email: 'Ingrese un correo valido'
            },
            rut: {
                required: 'Ingrese su rut',
                pattern: 'Ingrese un rut valido (0-9) & K',
                minlength: 'Debe contener minimo 8 caracteres',
                maxlength: 'Debe contener maximo 9 caracteres',
                rut: 'Rut invalido'
            },
            nombre: {
                required: 'Ingrese su nombre',
                pattern: 'Nombre solo puede contener letras'
            },
            contrasena: {
                required: 'Debe ingresar una contraseña',
                minlength: 'Contraseña debe tener minimo 8 caracteres'
            },
            fechanacimiento: {
                required: 'Debe ingresar su fecha de nacimiento',
                max: 'Debes ser mayor de 18 años'
            },
            telefono: {
                required: 'Debe ingresar su numero celular',
                pattern: 'Debe ingresar solo numeros',
                minlength: 'Debe contener al menos 8 numeros',
                maxlength: 'Debe contener maximo 8 numeros'
            },
            region: {
                required: 'Debe seleccionar una region'
            },
            comuna: {
                required: 'Debe seleccionar una comuna'
            },
            tipovivienda: {
                required: 'Debe seleccionar el tipo de vivienda'
            }
        },
    })
/*
    $("#formulario").submit(function () {
        if (Fn.validaRut($("#rut").val())) {
            console.log("RUT CORRECTO")
            return true;
        } else {
            console.log("RUT INCORRECTO")
            return false;
        }
    })
*/

    var Fn = {
        validaRut: function (rutCompleto) {
            var tmp = rutCompleto;
            var digv = tmp.substr(tmp.length-1);
            /*console.log(tmp.length-1)*/
            var rut = tmp.substr(0, tmp.length-1);
            if (digv == 'K') digv = 'k';
            return (Fn.dv(rut) == digv);
        },
        dv: function (T) {
            var M = 0, S = 1;
            for (; T; T = Math.floor(T / 10))
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
            return S ? S - 1 : 'k';
        }

    }

    jQuery.validator.addMethod("rut", function (value, element) {
        return this.optional(element) || Fn.validaRut(value);
    }, "Rut invalido");

})