/*
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./static/js/sw.js')
      .then(reg => console.log('Registro de SW exitoso', reg))
      .catch(err => console.warn('Error al tratar de registrar el sw'))
  }
*/
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('/sw.js')
        .then(function() {
            console.log('ServiceWorker registered!');
        })
        .catch(function(err) {
            console.log('ServiceWorker failed :(', err);
        });
    });
  }


$(function(){

    regiones.regiones.forEach(region => {
        $("#region").append('<option value="'+region.region+'">'+region.region+'</option>')
    });

})

$("#region").change(function(){
    $("#comuna").html("")
    var region = $(this).val()
    var comunas = regiones.regiones.find(item => item.region == region)
    comunas.comunas.sort().forEach(comuna =>{
        $("#comuna").append('<option value="'+comuna+'">'+comuna+'</option>')
    })
})




/*
-- load template

$(function(){
    $("#includedContent").load("b.html"); 
  });
*/



var regiones = (function() {
    var regiones = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': "/static/json/regiones.json",
        'dataType': "json",
        'success': function (data) {
            regiones = data;
        }
    });
    return regiones;
})();

