$(function () {
    $("#formnewpass").validate({
        rules: {
            newpass: {
                required: true,
                minlength: 8
            },
        },
        messages: {
            newpass: {
                required: 'Debe ingresar una contraseña',
                minlength: 'Contraseña debe tener minimo 8 caracteres'
            }
        },
    })
})