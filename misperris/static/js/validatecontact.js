$(function () {
    $("#formulario").validate({
        rules: {
            correo: {
                required: true,
                email: true
            },
            mensaje: {
                required: true,
                minlength: 30
            }
        },
        messages: {
            correo: {
                required: 'Debe ingresar su correo',
                email: 'Ingrese un correo valido'
            },
            mensaje: {
                required: 'Debe ingresar un mensaje',
                minlength: 'Mensaje especificado es muy corto'
            }
        },
    })
})