from django.apps import AppConfig


class CachedsiteConfig(AppConfig):
    name = 'cachedsite'
