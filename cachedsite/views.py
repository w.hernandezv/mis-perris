from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'cindex.html', {})

def quiensomos(request):
    return render(request, 'cquiensomos.html', {})