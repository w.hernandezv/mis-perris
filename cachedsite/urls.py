from django.contrib import admin
from django.urls import path, include
from . import views
from django.views.generic.base import TemplateView

from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', views.index, name='index'),
    path('quiensomos/', views.quiensomos, name='quiensomos')
]
