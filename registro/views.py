from django.shortcuts import render
from django.http import HttpResponse
from .models import Profile, Perro

from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from django.http import HttpResponseRedirect


def registro(request):
    
    return render(request, 'registro.html', {'usuarios':User.objects.all()})

def crear(request):
    rut = request.POST.get('rut','')
    nombre = request.POST.get('nombre','')
    contrasena = request.POST.get('contrasena','')
    correo = request.POST.get('correo','')
    telefono = request.POST.get('telefono','')
    fechanacimiento = request.POST.get('fechanacimiento','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    tipovivienda = request.POST.get('tipovivienda','')
    user = User.objects.create_user(username = rut, email = correo, password = contrasena, first_name = nombre)

    a = User.objects.get(username = rut)
    a.profile.telefono = telefono
    a.profile.fechanacimiento = fechanacimiento
    a.profile.region = region
    a.profile.comuna = comuna
    a.profile.tipovivienda = tipovivienda
    a.save()

    return HttpResponse('Usuario registrado')

def iniciar(request):
    rut = request.POST.get('rut','')
    contrasena = request.POST.get('contrasena','')
    user = authenticate(username=rut, password=contrasena)

    if user is not None:
        #Redirecciona a la pagina que manda el POST
        login(request, user)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse('rut o password incorrectos')

def cerrarsesion(request):
    logout(request)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def eliminarcuenta(request):
    username = request.session['usuario']
    u = User.objects.get(username = username)
    u.delete()
    del request.session['usuario']
    del request.session['nombre']
    return HttpResponse('Usuario eliminado')

def perfil(request, rut):
    try:
        a = User.objects.get(username = rut)
        adoptados = Perro.objects.all()
        usuario = request.session.get('usuario',None)
        nombre = request.session.get('nombre',None)
        if nombre == '':
            nombre = usuario
        return render(request, 'perfil.html', {'usuario':usuario, 'nombre':nombre, 'a':a, 'adoptados':adoptados,'rut':rut})
    except:
        return HttpResponse('Perfil no encontrado.')

def perros(request):
    usuario = request.session.get('usuario',None)
    nombre = request.session.get('nombre',None)
    if nombre == '':
        nombre = usuario
    return render(request, 'perros.html',{'perros':Perro.objects.all(),'usuario':usuario,'nombre':nombre,'s':'selected'})

def registroperro(request):
    return render(request, 'registroperro.html', {})

def crearperro(request):
    foto = request.FILES.get('foto')
    nombre = request.POST.get('nombre')
    raza = request.POST.get('raza')
    descripcion = request.POST.get('descripcion')
    estado = request.POST.get('estado')
    
    perro = Perro(foto = foto, nombre = nombre, raza = raza, descripcion =  descripcion, estado = estado)
    perro.save()
    return redirect('/perros/')

def eliminarperro(request, id):
    username = request.session.get('usuario', None)
    if username == 'sysadmin':
        u = Perro.objects.get(id = id)
        u.delete()
        return redirect('/perros/')
    else:
        return HttpResponse('Debes estar logeado como admin')

def editarperro(request, id):
    u = Perro.objects.get(id = id)
    foto = request.FILES.get(id + 'foto')
    nombre = request.POST.get(id +'nombre')
    raza = request.POST.get(id +'raza')
    descripcion = request.POST.get(id + 'descripcion')
    estado = request.POST.get(id + 'estado')
    
    if foto is not None:
        u.foto = foto
    u.nombre = nombre
    u.raza = raza
    u.descripcion = descripcion
    u.estado = estado
    u.save()
    return redirect('/perros/')

def quitarfoto(request, rut):
    if request.user.username:
        username = request.user.username
    if username == rut:
        u = User.objects.get(username = rut)
        u.profile.foto = None
        u.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse('No puedes quitar la foto de un perfil que no sea el tuyo.')

def subirfoto(request, rut):

    a = User.objects.get(username = rut)

    if a.username == rut:
        u = User.objects.get(username = rut)
        u.profile.foto = request.FILES.get('foto')
        u.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse('No puedes cambiar la foto de un perfil que no sea el tuyo.')

def cambiarcontrasena(request, rut):
    username = request.session['usuario']

    if username == rut:

        if request.POST.get('newpass') is None:
            return HttpResponse('Debes ingresar una nueva contrasena')
        u = User.objects.get(username = username)
        u.set_password(request.POST.get('newpass'))
        u.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse('No puedes realizar esta accion.')
        
def adoptarperro(request, id):
    if request.user.is_authenticated:
        username = request.user.username
        p = Perro.objects.get(id = id)
        u = User.objects.get(username = username)
        if p.estado == 'disponible':
            p.dueno = u
            p.estado = 'adoptado'
            p.save()
            return HttpResponse('Perro adoptado exitosamente!')
        else:
            return HttpResponse('Ese perro ya tiene un dueno')
    else:
        return HttpResponse('Debes estar logeado para realizar esta accion!')

# Cierra sesion, y luego intenta logear con facebook
def dontactdumb(request):
    logout(request)
    return redirect('/oauth/login/facebook/')