from django.urls import path
from . import views
from django.contrib.auth.views import LoginView

from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('registro/',views.registro, name='registro'),
    path('registro/crear/', views.crear, name='crear'),
    path('login/iniciar/',views.iniciar,name='iniciar'),
    path('cerrarsesion/', views.cerrarsesion, name='cerrarsesion'),
    path('eliminarcuenta/', views.eliminarcuenta, name='eliminarcuenta'),
    path('perfil/<str:rut>/', views.perfil, name='perfil'),
    path('perros/', views.perros, name='perros'),
    path('perros/crear/', views.crearperro, name='crearperro'),
    path('perros/eliminar/<int:id>', views.eliminarperro, name='eliminarperro'),
    path('perros/editar/<str:id>', views.editarperro, name='editarperro'),
    path('perfil/<str:rut>/quitarfoto/', views.quitarfoto, name='quitarfoto'),
    path('perfil/<str:rut>/subirfoto/', views.subirfoto, name='subirfoto'),
    path('perfil/<str:rut>/cambiarcontrasena/', views.cambiarcontrasena, name='cambiarcontrasena'),
    path('perros/adoptar/<int:id>', views.adoptarperro, name='adoptarperro'),
    path('dontactdumb/', views.dontactdumb, name='dontactdumb'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)