from django.contrib import admin

from .models import Profile, Perro

admin.site.register(Profile)
admin.site.register(Perro)