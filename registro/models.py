from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telefono = models.IntegerField(null=True, blank=True)
    fechanacimiento = models.DateField( null=True, blank=True)
    region = models.CharField(max_length=100, null=True, blank=True)
    comuna = models.CharField(max_length=100, null=True, blank=True)
    tipovivienda = models.CharField(max_length=100, null=True, blank=True)
    foto = models.ImageField(upload_to='profile_photo', null=True, blank=True)
    def __str__(self):
        return self.user.first_name

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

ESTADO_CHOICES = (
    ('rescatado','RESCATADO'),
    ('disponible', 'DISPONIBLE'),
    ('adoptado','ADOPTADO'),
)

class Perro(models.Model):
    foto = models.ImageField(upload_to='dog', blank=True, null=True)
    nombre = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)
    estado = models.CharField(max_length=100, choices=ESTADO_CHOICES, default='rescatado')
    dueno = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.nombre