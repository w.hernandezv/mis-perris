# Generated by Django 2.1.2 on 2018-10-20 06:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0007_perro_foto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perro',
            name='foto',
            field=models.FileField(blank=True, upload_to=''),
        ),
    ]
